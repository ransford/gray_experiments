import approxcode
import bitstring
import argparse
from itertools import permutations
from copy import copy

def neighbors(bits, distance, ungray=False):
    "All neighbors (Hamming distance=N) of a bitstring.BitArray"
    neigh = []

    # create a bitmask with 'distance' bits set, to permute
    bools = [True,] * distance
    bools.extend((False,) * (len(bits) - distance))

    for m in set(permutations(bools, len(bits))):
        mask = bitstring.BitArray(auto=m)
        c = copy(bits)
        for i, bitval in enumerate(c):
            c[i] = bitval ^ mask[i]
        if ungray:
            neigh.append(approxcode.gray2bin(c).uint)
        else:
            neigh.append(c.uint)

    return neigh

def avgneighbordistance(i, neigh):
    dist = map(lambda x: x - i, neigh)
    return sum(dist) / len(neigh)

def main(args):
    N = args.bitwidth
    D = args.distance
    print('N,err_bin,err_gray')
    for i in range(2**N):
        bits = bitstring.BitArray(length=N, uint=i)
        gray = approxcode.bin2gray(bits)
        print('{0},{1:0.5f},{2:0.5f}'.format(i,
            avgneighbordistance(i, neighbors(bits, distance=D)),
            avgneighbordistance(i, neighbors(gray, distance=D, ungray=True))))

if __name__ == '__main__':
    p = argparse.ArgumentParser()
    p.add_argument('bitwidth', type=int)
    p.add_argument('distance', type=int)
    args = p.parse_args()
    main(args)
